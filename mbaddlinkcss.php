<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*------------------------------------------------------------------------
# plg_mb-addlinkcss - Morbriant Add Link CSS
# Basado en el plugin 'Add Custom CSS' de impression-studio.
# ------------------------------------------------------------------------
# version 1.0.1
# author artbriant
# copyright Copyright (C) 2020 artbriant. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: (Original: http://joomla.impression-estudio.gr)
# Technical Support: artbriant@gmail.com (Original: info@impression-estudio.gr)
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgSystemMBAddLinkCSS extends JPlugin
{	
	function __construct( $subject, $params )
	{
		parent::__construct($subject, $params);

	}
	
	function onBeforeCompileHead()
	{
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		
		// **********************************************************************
		// Link CSS 1.
		// **********************************************************************
		if (!empty($this->params->get('link_css_file_path_1')))
		{
			$link_css_file_mode_1 = $this->params->get('link_css_file_mode_1');
			if (($app->isSite() && strcmp($link_css_file_mode_1, 'front-end')==0) || ($app->isAdmin() && strcmp($link_css_file_mode_1, 'back-end')==0) || strcmp($link_css_file_mode_1, 'both')==0)
			{
/*				$link_css_file_path_1 = JPATH_ROOT.'/'.$this->params->get('link_css_file_path_1'); */

				$link_css_file_path_1 = JURI::root( true ).'/'.$this->params->get('link_css_file_path_1');
/*
				ob_start();
				
				include($link_css_file_path_1);
				$styles = ob_get_contents();
				
				// Remove comments.
				if ($this->params->get('remove_comments'))
				{
					$styles = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/    /*   !', '', $styles);
				}
				
				// Minimize.
				if ($this->params->get('minimize'))
				{
					$styles = str_replace(array("\r\n", "\r", "\n", "\t"), '', $styles);
					$styles = preg_replace('/ +/', ' ', $styles);	// Replace multiple spaces with single space.
					$styles = trim($styles);		// Trim the string of leading and trailing space.
				}
				
				// Convert short absolute paths to full absolute paths.
				$styles = str_replace("url('", "url(", $styles);
				$styles = str_replace("')", ")", $styles);
				$styles = str_replace('url("', 'url(', $styles);
				$styles = str_replace('")', ')', $styles);
				$styles = str_replace('url(', 'url('.JURI::base(), $styles);
				
				ob_clean();
				//$document->addStyleDeclaration($styles);
				$document->addLinkTag('<style type="text/css">'.$styles.'</style>');
*/
				$document->addStyleSheet($link_css_file_path_1);
			}
		}
		
		// **********************************************************************
		// Link CSS 2.
		// **********************************************************************
		if (!empty($this->params->get('link_css_file_path_2')))
		{
			$link_css_file_mode_2 = $this->params->get('link_css_file_mode_2');
			if (($app->isSite() && strcmp($link_css_file_mode_2, 'front-end')==0) || ($app->isAdmin() && strcmp($link_css_file_mode_2, 'back-end')==0) || strcmp($link_css_file_mode_2, 'both')==0)
			{
/*				$link_css_file_path_2 = JPATH_ROOT.'/'.$this->params->get('link_css_file_path_2'); */

				$link_css_file_path_2 = JURI::root( true ).'/'.$this->params->get('link_css_file_path_2');

/*
				ob_start();
				include($custom_css_file_path_2);
				$styles = ob_get_contents();
				
				// Remove comments.
				if ($this->params->get('remove_comments'))
				{
					$styles = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/   /*  !', '', $styles);
				}
				
				// Minimize.
				if ($this->params->get('minimize'))
				{
					$styles = str_replace(array("\r\n", "\r", "\n", "\t"), '', $styles);
					$styles = preg_replace('/ +/', ' ', $styles);	// Replace multiple spaces with single space.
					$styles = trim($styles);		// Trim the string of leading and trailing space.
				}
				
				// Convert short absolute paths to full absolute paths.
				$styles = str_replace("url('", "url(", $styles);
				$styles = str_replace("')", ")", $styles);
				$styles = str_replace('url("', 'url(', $styles);
				$styles = str_replace('")', ')', $styles);
				$styles = str_replace('url(', 'url('.JURI::base(), $styles);
				
				ob_clean();
				//$document->addStyleDeclaration($styles);
				$document->addCustomTag('<style type="text/css">'.$styles.'</style>');
*/
				$document->addStyleSheet($link_css_file_path_2);

			}
		}
		
		// **********************************************************************
		// Link CSS 3.
		// **********************************************************************
		if (!empty($this->params->get('link_css_file_path_3')))
		{
			$link_css_file_mode_3 = $this->params->get('link_css_file_mode_3');
			if (($app->isSite() && strcmp($link_css_file_mode_3, 'front-end')==0) || ($app->isAdmin() && strcmp($link_css_file_mode_3, 'back-end')==0) || strcmp($link_css_file_mode_3, 'both')==0)
			{
/*				$link_css_file_path_3 = JPATH_ROOT.'/'.$this->params->get('link_css_file_path_3'); */

				$link_css_file_path_3 = JURI::root( true ).'/'.$this->params->get('link_css_file_path_3');

/*
				ob_start();
				include($custom_css_file_path_3);
				$styles = ob_get_contents();
				
				// Remove comments.
				if ($this->params->get('remove_comments'))
				{
					$styles = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/   /* !', '', $styles);
				}
				
				// Minimize.
				if ($this->params->get('minimize'))
				{
					$styles = str_replace(array("\r\n", "\r", "\n", "\t"), '', $styles);
					$styles = preg_replace('/ +/', ' ', $styles);	// Replace multiple spaces with single space.
					$styles = trim($styles);		// Trim the string of leading and trailing space.
				}
				
				// Convert short absolute paths to full absolute paths.
				$styles = str_replace("url('", "url(", $styles);
				$styles = str_replace("')", ")", $styles);
				$styles = str_replace('url("', 'url(', $styles);
				$styles = str_replace('")', ')', $styles);
				$styles = str_replace('url(', 'url('.JURI::base(), $styles);
				
				ob_clean();
				//$document->addStyleDeclaration($styles);
				$document->addCustomTag('<style type="text/css">'.$styles.'</style>');
*/
				$document->addStyleSheet($link_css_file_path_3);

			}
		}

		// **********************************************************************
		// Link CSS 4.
		// **********************************************************************
		if (!empty($this->params->get('link_css_file_path_4')))
		{
			$link_css_file_mode_4 = $this->params->get('link_css_file_mode_4');
			if (($app->isSite() && strcmp($link_css_file_mode_4, 'front-end')==0) || ($app->isAdmin() && strcmp($link_css_file_mode_4, 'back-end')==0) || strcmp($link_css_file_mode_4, 'both')==0)
			{
/*				$link_css_file_path_4 = JPATH_ROOT.'/'.$this->params->get('link_css_file_path_4'); */

				$link_css_file_path_4 = JURI::root( true ).'/'.$this->params->get('link_css_file_path_4');

/*
				ob_start();
				include($custom_css_file_path_4);
				$styles = ob_get_contents();
				
				// Remove comments.
				if ($this->params->get('remove_comments'))
				{
					$styles = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/   /* !', '', $styles);
				}
				
				// Minimize.
				if ($this->params->get('minimize'))
				{
					$styles = str_replace(array("\r\n", "\r", "\n", "\t"), '', $styles);
					$styles = preg_replace('/ +/', ' ', $styles);	// Replace multiple spaces with single space.
					$styles = trim($styles);		// Trim the string of leading and trailing space.
				}
				
				// Convert short absolute paths to full absolute paths.
				$styles = str_replace("url('", "url(", $styles);
				$styles = str_replace("')", ")", $styles);
				$styles = str_replace('url("', 'url(', $styles);
				$styles = str_replace('")', ')', $styles);
				$styles = str_replace('url(', 'url('.JURI::base(), $styles);
				
				ob_clean();
				//$document->addStyleDeclaration($styles);
				$document->addCustomTag('<style type="text/css">'.$styles.'</style>');
*/
				$document->addStyleSheet($link_css_file_path_4);

			}
		}

		// **********************************************************************
		// Link CSS 5.
		// **********************************************************************
		if (!empty($this->params->get('link_css_file_path_5')))
		{
			$link_css_file_mode_5 = $this->params->get('link_css_file_mode_5');
			if (($app->isSite() && strcmp($link_css_file_mode_5, 'front-end')==0) || ($app->isAdmin() && strcmp($link_css_file_mode_5, 'back-end')==0) || strcmp($link_css_file_mode_5, 'both')==0)
			{
/*				$link_css_file_path_5 = JPATH_ROOT.'/'.$this->params->get('link_css_file_path_5'); */

				$link_css_file_path_5 = JURI::root( true ).'/'.$this->params->get('link_css_file_path_5');

/*
				ob_start();
				include($custom_css_file_path_5);
				$styles = ob_get_contents();
				
				// Remove comments.
				if ($this->params->get('remove_comments'))
				{
					$styles = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/   /* !', '', $styles);
				}
				
				// Minimize.
				if ($this->params->get('minimize'))
				{
					$styles = str_replace(array("\r\n", "\r", "\n", "\t"), '', $styles);
					$styles = preg_replace('/ +/', ' ', $styles);	// Replace multiple spaces with single space.
					$styles = trim($styles);		// Trim the string of leading and trailing space.
				}
				
				// Convert short absolute paths to full absolute paths.
				$styles = str_replace("url('", "url(", $styles);
				$styles = str_replace("')", ")", $styles);
				$styles = str_replace('url("', 'url(', $styles);
				$styles = str_replace('")', ')', $styles);
				$styles = str_replace('url(', 'url('.JURI::base(), $styles);
				
				ob_clean();
				//$document->addStyleDeclaration($styles);
				$document->addCustomTag('<style type="text/css">'.$styles.'</style>');
*/
				$document->addStyleSheet($link_css_file_path_5);

			}
		}

	}
}


